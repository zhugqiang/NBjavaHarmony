package cn.harmony.base.network;



import cn.harmony.base.util.StringUtil;
import net.sf.json.JSONObject;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 发送http get请求，并接收数据
 */
public class HttpRequestUntil {

    /**
     * token中使用的活动ID
     */
    public static String MARK_ID = null;

    /**
     * 设置请求头
     */
    public static Map<String, String> getHttpHeader() {
        Map<String, String> headerMap = new HashMap<>();
//        headerMap.put("Content-Type", "application/json");
        headerMap.put("Accept", "application/json,");
        headerMap.put("Accept-Encoding", "UTF-8");
        headerMap.put("clienttype", "android");

        return headerMap;
    }

    /**
     * 获取params
     */
    public static Map<String, String> getParams(List<BasicNameValuePair> params) {
        Map<String, String> paramMap = getDefaultParams();
        if (params == null || params.size() < 1) {
            return paramMap;
        }
        for (BasicNameValuePair nameValuePair : params) {
            if (nameValuePair != null) {
                paramMap.put(StringUtil.getString(nameValuePair.getName()), StringUtil.getString(nameValuePair.getValue()));
            }
        }
        return paramMap;
    }

    /**
     * 获取默认的params
     */
    public static Map<String, String> getDefaultParams() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("token", makeToken());
        return paramMap;
    }

    public static String makeToken() {

        String token = "";
        String clienttype = "android";
        JSONObject jsonObject = new JSONObject();
        try {
            //客户端类型
            jsonObject.put("clienttype", clienttype);
            //app版本号
            jsonObject.put("version", getVersionName());
            if (StringUtil.isNotEmpty(MARK_ID)) {
                //活动id
                jsonObject.put("markid", MARK_ID);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        token = jsonObject.toString();
        return token;
    }


    /**
     * 获取应用版本号
     *
     * @return versionName
     */
    public static String getVersionName() {

        return "";
    }

}
