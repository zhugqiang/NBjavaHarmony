package cn.harmony.base.network;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

/**
 * 用于界面交互，无需操作toast提示。 msg.what 1和2已经被使用。
 *
 * @author lei.chuguang
 */
public abstract class DialogHandler extends EventHandler {
    public static final int dialogShow = 1;
    public static final int dialogDissmiss = 2;

    public DialogHandler(EventRunner runner) throws IllegalArgumentException {
        super(runner);
    }

    @Override
    protected void processEvent(InnerEvent msg) {
        super.processEvent(msg);
        switch (msg.eventId) {
            case dialogShow:
                dialogShow(msg.object.toString());
                break;
            case dialogDissmiss:
                dialogDissmiss(msg.object.toString());
                break;
            default:
                break;
        }
    }



    /**
     * 隐藏进度对话框
     *
     * @param msg 进度对话框显示的message
     */
    protected abstract void dialogDissmiss(String msg);

    /**
     * 显示进度对话框
     *
     * @param msg 进度对话框显示的message
     */
    protected abstract void dialogShow(String msg);
}
