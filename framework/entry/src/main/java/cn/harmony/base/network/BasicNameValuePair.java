package cn.harmony.base.network;

/**
 * @author Zyq
 * @date 2018/2/27　09:52.
 * 网络请求的参数键值对存储
 */

public class BasicNameValuePair {
    /**
     * 键值对德name
     */
    private String name;
    /**
     * 键值对value
     */
    private String value;

    public BasicNameValuePair(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }


}
