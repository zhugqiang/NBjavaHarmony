package cn.harmony.base.network;

/**
 *
 */
public abstract class NetDataHandle {
    /**
     * 联网之前的操作
     */
    public abstract void netBefore();

    /**
     * 联网成功
     *
     * @param json 获取到的数据
     */
    public abstract void netSuccessHanle(String json);

    /**
     * 联网失败没有区分code
     */
    public abstract void noDataHanle();

}
