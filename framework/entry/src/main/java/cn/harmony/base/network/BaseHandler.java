package cn.harmony.base.network;


import com.orhanobut.logger.Logger;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

public abstract class BaseHandler<T> extends DialogHandler {
    public static final int success = 3;
    public static final int fail = 4;
    public static final int error = 5;

    public BaseHandler(EventRunner runner) throws IllegalArgumentException {
        super(runner);
    }

    @Override
    public void processEvent(InnerEvent msg) {
        super.processEvent(msg);
        switch (msg.eventId) {
            case success:
                if (msg.object != null) {
                    try {
                        handleSuccessMessage((T) msg.object);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Logger.d("BaseHandler", e.getMessage());
                    }
                } else {
                    handleSuccessMessage(null);
                }
                break;
            case fail:
                if (msg.object == null) {
                    handleFailMessage("");
                } else {
                    try {
                        handleFailMessage((String) msg.object);
                    } catch (Exception ClassCastException) {
                        handleFailMessage("");
                    }
                }
            default:
                mHandleMessage(msg);
                break;
        }
    }

    /**
     * handleMessage(Message msg)方法扩展
     */
    protected void mHandleMessage(InnerEvent msg){

    }

    /**
     * 返回json能解析到T的信息处理
     */
    protected abstract void handleSuccessMessage(T t);

    /**
     * 返回失败信息处理
     *
     * @param msg 服务器返回的message。如果msg为null，表示server没有传值。
     */
    protected void handleFailMessage(String msg){

    }


    @Override
    protected void dialogShow(String msg) {

    }

    @Override
    protected void dialogDissmiss(String msg) {

    }
}
