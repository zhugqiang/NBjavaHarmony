package cn.harmony.shf.app.config;


/**
 * Config配置-Global
 *
 * @author Zyq
 */
public class GlobalUrlConfig {
    /**
     * 内部版本号
     */
    public static final String INNER_APP_VERSIONCODE = "6.0";
    /**
     * 开发环境
     */
    public static final String MAIN_HOST_wwwd = "https://wwwd.bus365.cn";
    /**
     * 测试环境
     */
    public static final String MAIN_HOST_wwwt = "https://wwwt.bus365.cn";
    /**
     * 测试2环境
     */
    public static final String MAIN_HOST_wwwt2 = "https://wwwt2.bus365.cn";
    /**
     * 模拟环境
     */
    public static final String MAIN_HOST_mraw = "https://mraw.bus365.cn";
    /**
     * 线上灰度发布环境
     */
    public static final String MAIN_HOST_onlinesinum = "https://onlinesinum.bus365.com";
    /**
     * 正式环境
     */
    public static final String MAIN_HOST_www = "https://www.bus365.com";
    /**
     * ture 准备上线，不能切换环境，fasle 开发or测试阶段，可更改环境
     */
    public static final boolean readyOnline = false;
    /**
     * 当前环境
     */
    public static String CurrentMainHost = MAIN_HOST_www;
    /**
     * 快行服务器
     */
    public static String HOST = CurrentMainHost + "/qtrip/";
    /**
     * http://192.168.0.121:8901
     * 网约车服务器
     */
    public static String HOST_WYC = CurrentMainHost + "/wyc/";
    /**
     * 汽车票服务器
     */
    public static String COACH_MAIN_HOST = CurrentMainHost + "/";
    /**
     * 火车票、订单服务器
     * interface_app 服务器
     */
    public static String TRAIN_HOST = CurrentMainHost + "/bus/interface/";
    /**
     * 畅游服务器
     */
    public static String TRAVEL_HOST = CurrentMainHost + "/";

    /**
     * 配置网络超时时间 毫秒
     * 默认30秒
     */
    public static long HTTP_TIMEOUT = 30000L;
    /**
     * 业务代码/业务场景 businesscode 机场巴士：jcbs
     * 校园巴士：xybs 景点巴士：jdbs 周边游：zby 门票：mp
     */
    /**
     * 汽车票
     */
    public static final String businesscode_qcp = "bus";
    /**
     * 门票
     */
    public static final String businesscode_mp = "mp";
    /**
     * 实时打车
     */
    public static final String businesscode_wykc = "wykc";
    /**
     * 预约打车
     */
    public static final String businesscode_yydc = "yydc";
    /**
     * 城际用车
     */
    public static final String businesscode_cjyc = "cjyc";
    /**
     * 预约用车
     */
    public static final String businesscode_yyyc = "yyyc";
    /**
     * 机场用车
     */
    public static final String businesscode_jcyc = "jcyc";
    /**
     * 专线
     */
    public static final String businesscode_zx = "zx";
    /**
     * 巴士
     */
    public static final String businesscode_bs = "bs";
    /**
     * 专线巴士
     */
    public static final String businesscode_zxbs = "zxbs";
    /**
     * 机场巴士
     */
    public static final String businesscode_jcbs = "jcbs";
    /**
     * 校园巴士
     */
    public static final String businesscode_xybs = "xybs";
    /**
     * 景点巴士
     */
    public static final String businesscode_jdbs = "jdbs";
    /**
     * 专线-定制巴士
     */
    public static final String businesscode_dzbs = "dzbs";
    /**
     * 专线-城际快车
     */
    public static final String businesscode_cjkc = "cjkc";
    /**
     * 火车票
     */
    public static final String businesscode_hcp = "train";
    /**
     * 打车-同行
     */
    public static final String businesscode_cjpc = "cjpc";
    /**
     * 同行-包车
     */
    public static final String businesscode_cjpcbc = "cjpcbc";
    /**
     * 同行-拼车
     */
    public static final String businesscode_cjpcpc = "cjpcpc";
    /**
     * 打车-聚合快车
     */
    public static final String businesscode_wydc = "jhkc";
    /**
     * 出租车
     */
    public static final String businesscode_czc = "czc";
    /**
     * 专线
     */
    public static final String businesscode_dzzx = businesscode_jcbs + ',' + businesscode_xybs + ',' + businesscode_jdbs + ',' + businesscode_dzbs;

    /**
     * 是否显示mock数据
     * 0真实数据，1假数据
     */
    public static String ISMOCK = "0";
    /**
     * logo图片地址
     */
    public static String LOGO_PATH = "/public/www/images/logo.png";

    /**
     * 首页素材入口标题信息获取
     */
    public final static String getIndexContentInfo = "/discoveryMaterial/getIndexContentInfo";
    /**
     * 首页获取公告
     */
    public final static String getNoticeInformation = "/bus/interface/app/v6.0/common/getnoticeinformation";

    /**
     * 首页城市选择所有城市
     */
    public static String getHomeCity = "country/searchcity";

    /**
     * 门票查询所有城市
     */
    public static String getCityCnByEn = "travel/interface/bms/getCityCnByEn";
    /**
     * 旅游首页查询所有城市
     */
    public static String getTourismCityCnByEn = "travel/interface/bms/v4.0/getCityCnByEn";

    /**
     * 门票查询热门城市
     */
    public static String getRegionHotByTypeMp = "travel/interface/bms/getRegionHotByType";
    /**
     * 旅游首页查询热门城市
     */
    public static String getTourismRegionHotByType = "travel/interface/bms/v4.0/getRegionHotByType";
    /**
     * 获取订单列表
     */
    public final static String getorderlist = "bus/interface/order/getorderlistv2";
    /**
     * 我的行程、历史行程
     */
    public static final String myTripInfo = "itinerary/myTripInfo";
    /**
     * 删除历史行程
     */
    public static final String updateShowHistoryTrip = "itinerary/updateShowHistoryTrip";
    /**
     * 电子票列表
     */
    public final static String geteticketlist = "bus/interface/eticket/list";
    /**
     * 专线班次列表推荐接口 bus/interface
     */
    public static String reproduct = "schedulelist/reproduct";


    /**
     * 5.10.74	我的积分、成长值明细查询接口
     */
    public final static String scoregrowdetails = "user/scoregrowdetails" + ISMOCK;
    /**
     * 5.10.76	获取订单点评列表
     */
    public final static String getorderevaluate = "order/getcommentorderlist";
    /**
     * 首页推荐
     */
    public static String recommendterms = "/recommendterms" + ISMOCK;
    /**
     * 是否有推荐班次(业务标签判断)接口
     */
    public final static String APP_GET_LIST_MARK = "/bus/interface/commoninterface/getappbookmark";
    /**
     * 获取车站最大预售期 commoninterface/getpresaleperiod
     */
    public final static String APP_GET_CALENDAR = "/bus/interface/app/v6.0/common/getcalendar";
    /**
     * 获取乘车人小程序分享参数接口
     */
    public final static String getMiniShareToAddPassenger = "/bus/interface/app/v6.0/common/getminisharetoaddpassenger";
    /**
     * 获取珊瑚广告开启的位置信息
     */
    public final static String getShanHuAdEnablePosition = "/bus/interface/app/v6.0/common/getshanhuadenableposition";
    /**
     * 电子发票
     */
    public static String elecinvoice = "/public/www/elecinvoice4/elecinvoice-index.html";
    /**
     * 金币列表页面
     */
    public static String showmoney = "/public/www/promotion/promotion-showmoney.html?iscore=0";

    /**
     * 用户协议
     */
    public static String useragreement = "/public/www/privacy4/userprotocol.html";
    /**
     * 隐私政策
     */
    public static String privacypolicy = "/public/www/privacy4/privacy-policy.html";

    /**
     * -----------------------------------------下面为城际用车接口---------------------------------
     *
     *
     * 城际用车乘车协议查询接口
     */
    public final static String qtrip_getAgreementUrl = "qtrip_getAgreementUrl" + ISMOCK;
    /**
     * 取消订单原因查询接口
     */
    public final static String qtrip_getcancelorderreason = "qtrip_getcancelorderreason" + ISMOCK;
    /**
     * 查询订单
     */
    public static String SEACHORDERS = "specialcar/seachorders" + ISMOCK;
    /**
     * 查询待评价订单
     */
    public static String SEACHORDERSS = "specialcar/seachorders" + ISMOCK;
    /**
     * 取消订单
     */
    public static String CANCELORDER = "specialcar/cancelorder" + ISMOCK;
    /**
     * 提交评价
     */
    public static String EVALUATEORDER = "specialcar/saveassessment" + ISMOCK;
    /**
     * 获取支付网关
     */
    public static String GETPAYWAYS = "specialcar/gateways" + ISMOCK;
    /**
     * 创建支付参数
     */
    public static String GETPAYPARAMS = "specialcar/payparam" + ISMOCK;
    /**
     * 机构代码
     */
    public static String findVehiclebrandByVttypeid = "vehiclebrand/findVehiclebrandByVttypeid";
    /**
     * 查询订单详情
     */
    public static String SEACHORDERDETAIL = "specialcar/seachorderdetail" + ISMOCK;
    /**
     * 查询订单评价
     */
    public static String EVLUATEORDERQUERY = "specialcar/queryassessment" + ISMOCK;
    /**
     * 获取方位
     */
    public static String getDirection = "direction" + ISMOCK;
    // 出发城市接口
    public static String hotCitys = "specialcar/topcities" + ISMOCK;
    public static String departcitys_url = "specialcar/departcities" + ISMOCK;
    public static String reachcities = "specialcar/reachcities" + ISMOCK;
    /**
     * 已开通出发城市
     */
    public final static String openeddepartcity = "qtrip_zc_/openeddepartcity" + ISMOCK;
    /**
     * 已开通出发城市和出发地v2.0
     */
    public final static String cjyc_openeddepartarea_V2 = "app/V2.0/cjyc_openeddepartarea" + ISMOCK;
    /**
     * 百度地图地点建议接口v2.0
     */
    public final static String cjyc_placesuggestion_adress = "app/V2.0/cjyc_placesuggestion_adress" + ISMOCK;

    /**
     * 已开通到达地v2.0
     */

    public final static String cjyc_openeddepartarea = "app/V2.0/cjyc_openedreacharea" + ISMOCK;

    /**
     * 线路查询接口v2.0
     */
    public final static String cjyc_routeByReturnEndStation = "app/V2.0/cjyc_routeByReturnEndStation" + ISMOCK;
    /**
     * 准备申请订单接口v1.0
     */
    public final static String cjyc_createorderprepare = "app/V1.0/cjyc_createorderprepare" + ISMOCK;
    /**
     * 3.7.14	获取支付网关v2.0
     */
    public final static String cjyc_gateways = "app/V2.0/cjyc_gateways" + ISMOCK;
    /**
     * 获取车辆位置信息
     */
    public static String PULLADDRESS = "specialcar/pulladdress0";

}