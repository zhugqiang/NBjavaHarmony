package cn.harmony.shf.example;

import cn.harmony.base.network.BaseHandler;
import cn.harmony.shf.ResourceTable;
import cn.harmony.shf.app.business.PhoneparaminfoServer;
import cn.harmony.shf.example.slice.NetWorkAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;

public class NetWorkAbility extends Ability {
    private Text text_helloworld;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(NetWorkAbilitySlice.class.getName());
        initView();
        getUpdateparams();
    }

    /**
     * 初始化View
     */
    private void initView() {

        text_helloworld=(Text)findComponentById(ResourceTable.Id_text_helloworld);


    }

    /**
     * 获取更新参数
     */
    private void getUpdateparams() {
        new PhoneparaminfoServer().getUpdateparams("101", new BaseHandler<String>(EventRunner.current()) {
            @Override
            protected void handleSuccessMessage(String updateString) {


                text_helloworld.setText(updateString);


            }

            @Override
            protected void handleFailMessage(String msg) {

            }
        });
    }
}
