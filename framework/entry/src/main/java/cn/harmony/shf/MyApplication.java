package cn.harmony.shf;

import cn.harmony.shf.app.config.GlobalUrlConfig;
import com.orhanobut.logger.HarmonyLogAdapter;
import com.orhanobut.logger.Logger;
import com.zhy.http.okhttp.OkHttpUtils;
import ohos.aafwk.ability.AbilityPackage;
import okhttp3.OkHttpClient;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import java.util.concurrent.TimeUnit;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {

        super.onInitialize();

        Logger.addLogAdapter(new HarmonyLogAdapter(){

            @Override
            public boolean isLoggable(int i, String s) {
                return true;
            }
        });
        initOkHttp();
    }
    private void initOkHttp() {
//ssl验证
//        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(null, null, null);
//.sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)
//Cookie
//        ClearableCookieJar cookieJar1 = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(getApplicationContext()));
//        CookieJarImpl cookieJar1 = new CookieJarImpl(new MemoryCookieStore());
//                .cookieJar(cookieJar1)
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        //                builder.addInterceptor(logging);
        //设置超时时间
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(GlobalUrlConfig.HTTP_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(GlobalUrlConfig.HTTP_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(GlobalUrlConfig.HTTP_TIMEOUT, TimeUnit.MILLISECONDS);
//        if (!GlobalUrlConfig.readyOnline) {
//            // 分别添加网络模拟拦截器
//            // 添加请求日志拦截器
//            builder.addNetworkInterceptor(new SimulateNetworkInterceptor())
//                    .addNetworkInterceptor(new RequestLoggerInterceptor());
//        }

        //https
        builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        }).retryOnConnectionFailure(true);
        OkHttpClient okHttpClient = builder.build();
        OkHttpUtils.initClient(okHttpClient);
    }
}
