package cn.harmony.shf;

import cn.harmony.base.network.BaseHandler;
import cn.harmony.shf.app.business.PhoneparaminfoServer;
import cn.harmony.shf.slice.MainAbilitySlice;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;


public class MainAbility extends Ability {

    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());


    }

}
