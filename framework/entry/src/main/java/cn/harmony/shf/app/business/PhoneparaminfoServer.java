package cn.harmony.shf.app.business;



import cn.harmony.base.network.BaseHandler;
import cn.harmony.base.network.BasicNameValuePair;
import cn.harmony.base.network.EndHandle;
import cn.harmony.base.network.NetDataHandle;
import cn.harmony.shf.app.config.GlobalUrlConfig;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orhanobut.logger.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.InnerEvent;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class PhoneparaminfoServer extends EndHandle {


    /**
     * 获取版本更新
     *
     * @param code
     * @param handler
     */
    public void getUpdateparams(String code, BaseHandler<String> handler) {
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("code", code));
        getUpdateparams(list, handler);
    }

    /**
     * 获取版本更新
     *
     * @param mList
     * @param handler
     */
    protected void getUpdateparams(List<BasicNameValuePair> mList, final EventHandler handler) {
        sendRequestRunnable(0, GlobalUrlConfig.COACH_MAIN_HOST + "phoneparam", mList, new NetDataHandle() {

            @Override
            public void noDataHanle() {
                InnerEvent msg =InnerEvent.get();
                msg.object = "";
                msg.eventId = BaseHandler.fail;
                handler.distributeEvent(msg);
            }

            @Override
            public void netSuccessHanle(String json) {
                try {
                    JSONArray jsonArray = new JSONArray(json);
                    JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                    String description = jsonObject.getString("description");
                    InnerEvent msg = InnerEvent.get();
                    msg.object = description;
                    msg.eventId = BaseHandler.success;
                    handler.distributeEvent(msg);
                } catch (Exception e) {
                    InnerEvent msg = InnerEvent.get();
                    msg.object = "";
                    msg.eventId = BaseHandler.fail;
                    handler.distributeEvent(msg);
                }
            }

            @Override
            public void netBefore() {
            }
        });
    }





}
