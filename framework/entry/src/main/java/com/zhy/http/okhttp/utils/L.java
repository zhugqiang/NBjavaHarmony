package com.zhy.http.okhttp.utils;


import com.orhanobut.logger.Logger;

/**
 * Created by zhy on 15/11/6.
 */
public class L {
    private static boolean debug = false;

    public static void e(String msg) {
        if (debug) {
            Logger.d("OkHttp", msg);
        }
    }

}

