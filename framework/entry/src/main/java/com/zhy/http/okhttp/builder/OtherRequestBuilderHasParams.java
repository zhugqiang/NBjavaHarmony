package com.zhy.http.okhttp.builder;

import com.zhy.http.okhttp.request.OtherRequest;
import com.zhy.http.okhttp.request.RequestCall;

import java.util.LinkedHashMap;
import java.util.Map;

import okhttp3.RequestBody;

/**
 * DELETE、PUT、PATCH等其他方法
 */
public class OtherRequestBuilderHasParams extends OkHttpRequestBuilder<OtherRequestBuilderHasParams> implements HasParamsable {
    private RequestBody requestBody;
    private String method;
    private String content;

    public OtherRequestBuilderHasParams(String method) {
        this.method = method;
    }

    @Override
    public RequestCall build() {
        return new OtherRequest(requestBody, content, method, url, tag, params, headers, id).build();
    }

    public OtherRequestBuilderHasParams requestBody(RequestBody requestBody) {
        this.requestBody = requestBody;
        return this;
    }

    public OtherRequestBuilderHasParams requestBody(String content) {
        this.content = content;
        return this;
    }

    @Override
    public OtherRequestBuilderHasParams params(Map<String, String> params) {
        this.params = params;
        return this;
    }

    @Override
    public OtherRequestBuilderHasParams addParams(String key, String val) {
        if (this.params == null) {
            params = new LinkedHashMap<>();
        }
        params.put(key, val);
        return this;
    }

}
