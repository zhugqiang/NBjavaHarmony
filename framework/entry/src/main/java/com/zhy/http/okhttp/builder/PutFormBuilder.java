package com.zhy.http.okhttp.builder;

import com.zhy.http.okhttp.request.PutFormRequest;
import com.zhy.http.okhttp.request.RequestCall;

import java.util.LinkedHashMap;
import java.util.Map;

import okhttp3.RequestBody;

/**
 * PUT
 */
public class PutFormBuilder extends OkHttpRequestBuilder<PutFormBuilder> implements HasParamsable {
    private RequestBody requestBody;
    private String method;
    private String content;

    public PutFormBuilder(String method) {
        this.method = method;
    }

    @Override
    public RequestCall build() {
        return new PutFormRequest(requestBody, content, method, url, tag, params, headers, id).build();
    }

    public PutFormBuilder requestBody(RequestBody requestBody) {
        this.requestBody = requestBody;
        return this;
    }

    public PutFormBuilder requestBody(String content) {
        this.content = content;
        return this;
    }

    @Override
    public PutFormBuilder params(Map<String, String> params) {
        this.params = params;
        return this;
    }

    @Override
    public PutFormBuilder addParams(String key, String val) {
        if (this.params == null) {
            params = new LinkedHashMap<>();
        }
        params.put(key, val);
        return this;
    }

}
