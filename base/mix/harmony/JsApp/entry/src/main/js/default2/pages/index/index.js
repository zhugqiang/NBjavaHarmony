import router from '@system.router';
export default {
    data: {
        title: ""
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    gotoNext:function (params) {

        router.push({
            uri: 'pages/page3/page3'
        });

   }
}
