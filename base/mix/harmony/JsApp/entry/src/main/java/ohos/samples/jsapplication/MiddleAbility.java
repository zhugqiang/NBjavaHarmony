package ohos.samples.jsapplication;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.rpc.IRemoteObject;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.app.Context;
import ohos.rpc.*;
import java.io.OutputStream;
import java.net.Socket;
public class MiddleAbility extends Ability {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "Demo");
    private static final int OPEN_NEWPAGE = 1001;
    private MiddleAbility.MideleRemote mideleRemote;
    @Override
    public void onStart(Intent intent) {
        HiLog.error(LABEL_LOG, "MiddleAbility::onStart");
        super.onStart(intent);
    }

    @Override
    public void onBackground() {
        super.onBackground();
        HiLog.info(LABEL_LOG, "MiddleAbility::onBackground");
    }

    @Override
    public void onStop() {
        super.onStop();
        HiLog.info(LABEL_LOG, "MiddleAbility::onStop");
    }

    @Override
    public void onCommand(Intent intent, boolean restart, int startId) {
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {

        Context context = getContext();
        mideleRemote = new MiddleAbility.MideleRemote(context);
        return mideleRemote.asObject();
    }

    @Override
    public void onDisconnect(Intent intent) {
    }
    class MideleRemote extends RemoteObject implements IRemoteBroker {

        private Context context;
        private Socket socket = null;
        private OutputStream os = null;

        public MideleRemote(Context context) {
            super("MideleRemote");
            this.context = context;
        }

        @Override
        public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) throws RemoteException {
            switch (code) {
                case OPEN_NEWPAGE:{
                    System.out.println("开启页面2");
                    //主要代码
                    Intent intent = new Intent();
                    Operation operation = new Intent.OperationBuilder().withBundleName(getBundleName())
                            .withAbilityName(MainAbility2.class.getName()).build();
                    intent.setOperation(operation);
                    startAbility(intent);

                    //主要代码

                    break;
                }
                default: {
                    reply.writeString("service not defined");
                    return false;
                }
            }
            return true;
        }

        @Override
        public IRemoteObject asObject() {
            return this;
        }
    }
}