/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.samples.commonlayout.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.TableLayout;
import ohos.agp.components.Text;
import ohos.samples.commonlayout.ResourceTable;

/**
 *
 */
public class TableLayoutAbilitySlice extends AbilitySlice {
    private TableLayout tableLayout;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_tableLayout_ability_slice);
        initComponents();
    }

    private void initComponents() {

        Text component = (Text) findComponentById(ResourceTable.Id_text_one);
        tableLayout= (TableLayout) findComponentById(ResourceTable.Id_tableLayout);

        TableLayout.LayoutConfig tlc = new TableLayout.LayoutConfig();
        tlc.columnSpec = TableLayout.specification(TableLayout.DEFAULT, 2, TableLayout.Alignment.ALIGNMENT_FILL);
        tlc.rowSpec = TableLayout.specification(TableLayout.DEFAULT, 2, TableLayout.Alignment.ALIGNMENT_FILL);
        component.setLayoutConfig(tlc);
    }
}
