/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.iwgang.familiarrecyclerviewdemo.util;

import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * DimenUtil
 */
public class DimenUtil {

    /**
     * 获取dimen
     *
     * @param context context
     * @param dimen dimen
     * @return int
     */
    public static int getDimenValue(Context context , int dimen) {
        int dimes = 0;
        DisplayAttributes da = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        dimes = Math.round(da.densityPixels * dimen);
        return dimes;
    }

    /**
     * Retrieves a dimensional for a particular resource ID for use as a size in raw pixels.
     *
     * @param context context
     * @param dimenRes the dimension resource identifier
     * @return int Resource dimension value multiplied by the appropriate metric and truncated to
     * integer pixels.
     */
    public static int getDimen(Context context , int dimenRes) {
        int dimes = 0;
        try {
            int vp = context.getResourceManager().getElement(dimenRes).getInteger();
            DisplayAttributes da = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
            dimes = Math.round(da.densityPixels * vp);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return dimes;
    }
}
