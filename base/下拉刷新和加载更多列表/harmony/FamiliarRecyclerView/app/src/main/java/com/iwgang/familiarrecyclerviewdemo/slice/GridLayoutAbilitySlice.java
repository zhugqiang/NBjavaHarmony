package com.iwgang.familiarrecyclerviewdemo.slice;

import com.iwgang.familiarrecyclerview.GridViewAdapter;
import com.iwgang.familiarrecyclerviewdemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GridLayoutAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private boolean           isVertical = true;
    private ArrayList<String> mDatas;
    private MyAdapter mAdapter;
    private ListContainer mRecyclerView;

    @Override
    protected void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_title_bar_bg).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        super.onStart(intent);
        if (intent != null) {
            isVertical = intent.getBooleanParam("flag", true);
        }
        if (isVertical) {
            super.setUIContent(ResourceTable.Layout_act_layout_grid_ver);
        } else {
            super.setUIContent(ResourceTable.Layout_act_layout_grid_hor);
        }
        initView();
    }

    private void initView() {
        mDatas = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            mDatas.add("item:" + i);
        }
        Component recycleView = findComponentById(ResourceTable.Id_grid_mRecyclerView);
        if (recycleView instanceof ListContainer) {
            mRecyclerView = (ListContainer) recycleView;
        }
        int layoutId = isVertical ? ResourceTable.Layout_item_view_grid :
                ResourceTable.Layout_item_view_grid_hor;
        mAdapter = new MyAdapter(this, mDatas, layoutId);
        if (isVertical) {
            mAdapter.setNumColumns(3);
        }else {
            mAdapter.setNumColumns(4);
        }
        // add headView
        addHeadView();
        // add footView
        addFootView();
        // set emptyView
        setEmptyView();
        mRecyclerView.setItemProvider(mAdapter);
        // add item
        Component add = findComponentById(ResourceTable.Id_tv_add);
        if (add instanceof Text) {
            Text tvAdd = (Text) add;
            tvAdd.setClickedListener(this);
        }
        // delete item
        Component delete = findComponentById(ResourceTable.Id_tv_delete);
        if (delete instanceof Text) {
            Text tvDelete = (Text) delete;
            tvDelete.setClickedListener(this);
        }
    }

    private void setEmptyView() {
        LayoutScatter mLayoutScatter3 = LayoutScatter.getInstance(this);
        if (isVertical) {
            mAdapter.setEmptyView(mLayoutScatter3
                    .parse(ResourceTable.Layout_empty_view , null , false));
        }else {
            mAdapter.setEmptyView(mLayoutScatter3
                    .parse(ResourceTable.Layout_empty_view_hor , null , false));
        }
    }

    private void addFootView() {
        LayoutScatter mLayoutScatter2 = LayoutScatter.getInstance(this);
        if (isVertical) {
            mAdapter.addFooterView(mLayoutScatter2
                    .parse(ResourceTable.Layout_view_footer_ver, null, false));
        }else {
            mAdapter.addFooterView(mLayoutScatter2
                    .parse(ResourceTable.Layout_view_footer_hor, null, false));
        }
    }

    private void addHeadView() {
        LayoutScatter mLayoutScatter = LayoutScatter.getInstance(this);
        if (isVertical) {
            mAdapter.addHeaderView(mLayoutScatter
                    .parse(ResourceTable.Layout_view_header_ver, null, false));
        }else {
            mAdapter.addHeaderView(mLayoutScatter
                    .parse(ResourceTable.Layout_view_header_hor, null, false));
        }
    }


    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackPressed() {
        terminateAbility();
        super.onBackPressed();
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_tv_add:
                if (mDatas != null) {
                    mDatas.add("new add item:" + mDatas.size());
                    mAdapter.refresh(true);
                }
                break;
            case ResourceTable.Id_tv_delete:
                if (mDatas.size() >= 1) {
                    mDatas.remove(mDatas.size() - 1);
                    mAdapter.refresh(true);
                }
                break;
        }
    }

    private static class MyAdapter extends GridViewAdapter<String> {
        public MyAdapter(Context context, List<String> data, int layoutId) {
            super(context, data, layoutId);
        }

        @Override
        public void convert(GridViewHolder holder, int[] positions, List<String> models) {
            for (int i = 0; i < positions.length; i++) {
                switch (i) {
                    case 0:
                        Text textView = holder.getTextView(ResourceTable.Id_tv_txt1);
                        textView.setText(models.get(i));
                        textView.setVisibility(Component.VISIBLE);
                        break;
                    case 1:
                        Text textView2 = holder.getTextView(ResourceTable.Id_tv_txt2);
                        textView2.setText(models.get(i));
                        textView2.setVisibility(Component.VISIBLE);
                        break;
                    case 2:
                        Text textView3 = holder.getTextView(ResourceTable.Id_tv_txt3);
                        textView3.setText(models.get(i));
                        textView3.setVisibility(Component.VISIBLE);
                        break;
                    case 3:
                        Text textView4 = holder.getTextView(ResourceTable.Id_tv_txt4);
                        textView4.setText(models.get(i));
                        textView4.setVisibility(Component.VISIBLE);
                        break;
                    default:
                        break;
                }
            }
        }

        @Override
        public void setNumColumns(int numColumns) {
            super.setNumColumns(numColumns);
        }

        @Override
        public void addHeaderView(Component header) {
            super.addHeaderView(header);
        }

        @Override
        public void addFooterView(Component footer) {
            super.addFooterView(footer);
        }

        @Override
        public void setEmptyView(Component component) {
            super.setEmptyView(component);
        }

        @Override
        public void refresh(boolean isAdd) {
            super.refresh(isAdd);
        }
    }

}
