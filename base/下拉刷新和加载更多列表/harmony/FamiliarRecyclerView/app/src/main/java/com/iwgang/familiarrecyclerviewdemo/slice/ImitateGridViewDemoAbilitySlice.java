package com.iwgang.familiarrecyclerviewdemo.slice;

import com.iwgang.familiarrecyclerview.FamiliarRefreshRecyclerView;
import com.iwgang.familiarrecyclerview.GridViewAdapter;
import com.iwgang.familiarrecyclerviewdemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImitateGridViewDemoAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private FamiliarRefreshRecyclerView mRecyclerView;
    private ArrayList<String>           mData;
    private MyAdapter                   mProvider;

    @Override
    protected void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_title_bar_bg).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_imitate_grid);
        initView();
    }

    private void initView() {
        mData = new ArrayList<>();
        mData.addAll(getData());
        Component recycleView = findComponentById(ResourceTable.Id_refresh_recycler_view);
        if (recycleView instanceof FamiliarRefreshRecyclerView) {
            mRecyclerView = (FamiliarRefreshRecyclerView) recycleView;
        }
        ListContainer listContainer = new ListContainer(this);
        listContainer.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        listContainer.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        mProvider = new MyAdapter(this, mData,
                ResourceTable.Layout_item_view_imitate_grid);
        mProvider.setNumColumns(3);

        // add headerView
        LayoutScatter mLayoutScatter = LayoutScatter.getInstance(this);
        mProvider.addHeaderView(mLayoutScatter
                .parse(ResourceTable.Layout_view_header_ver, null, false));

        // set EmptyView
        LayoutScatter mLayoutScatter3 = LayoutScatter.getInstance(this);
        mProvider.setEmptyView(mLayoutScatter3
                .parse(ResourceTable.Layout_empty_view , null , false));
        listContainer.setItemProvider(mProvider);

        // 因为ListContainer自带拖拽功能,这里屏蔽掉该事件才能禁止拖拽功能
        listContainer.setLongClickable(false);
        mRecyclerView.addContentView(listContainer);
        mRecyclerView.setDataSizes(mProvider.getCount());

        // 下拉刷新回调
        mRecyclerView.setOnPullRefreshListener(() ->
                new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> {
                    mData.clear();
                    mData.addAll(getData());
                    mProvider.refresh(true);
                    mRecyclerView.setDataSizes(mProvider.getCount());
                } , 1000));

        // 上拉加载回调
        mRecyclerView.setOnLoadMoreListener(() ->
                new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> {
                    int curMaxData = mData.size();
                    for (int i = 0; i < 10; i++) {
                        mData.add("item : " + (curMaxData + i));
                    }
                    mProvider.refresh(false);
                    mRecyclerView.setDataSizes(mProvider.getCount());
                }, 1000));
        Component add = findComponentById(ResourceTable.Id_tv_add);
        if (add instanceof Text) {
            Text tvAdd = (Text) add;
            tvAdd.setClickedListener(this);
        }
        Component delete = findComponentById(ResourceTable.Id_tv_delete);
        if (delete instanceof Text) {
            Text tvDelete = (Text) delete;
            tvDelete.setClickedListener(this);
        }
    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_tv_add:
                mData.add("new add item:" + mData.size());
                mProvider.refresh(true);
                mRecyclerView.setDataSizes(mProvider.getCount());
                break;
            case ResourceTable.Id_tv_delete:
                if (mData.size() >= 1) {
                    mData.remove(mData.size() - 1);
                    mProvider.refresh(true);
                    mRecyclerView.setDataSizes(mProvider.getCount());
                }
                break;
        }
    }


    private List<String> getData() {
        List<String> tempData = new ArrayList<>();
        int curMaxData = mData.size();
        for (int i = 0; i < 10; i++) {
            tempData.add("item : " + (curMaxData + i));
        }
        return tempData;
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackPressed() {
        terminateAbility();
        super.onBackPressed();
    }

    private static class MyAdapter extends GridViewAdapter<String> {
        public MyAdapter(Context context, List<String> data, int layoutId) {
            super(context, data, layoutId);
        }

        @Override
        public void convert(GridViewHolder holder, int[] positions, List<String> models) {
            for (int i = 0; i < positions.length; i++) {
                switch (i) {
                    case 0:
                        Text textView = holder.getTextView(ResourceTable.Id_tv_txt1);
                        textView.setText(models.get(i));
                        textView.setVisibility(Component.VISIBLE);
                        break;
                    case 1:
                        Text textView2 = holder.getTextView(ResourceTable.Id_tv_txt2);
                        textView2.setText(models.get(i));
                        textView2.setVisibility(Component.VISIBLE);
                        break;
                    case 2:
                        Text textView3 = holder.getTextView(ResourceTable.Id_tv_txt3);
                        textView3.setText(models.get(i));
                        textView3.setVisibility(Component.VISIBLE);
                        break;
                    default:
                        break;
                }
            }
        }

        @Override
        public void setNumColumns(int numColumns) {
            super.setNumColumns(numColumns);
        }

        @Override
        public void addHeaderView(Component header) {
            super.addHeaderView(header);
        }

        @Override
        public void addFooterView(Component footer) {
            super.addFooterView(footer);
        }

        @Override
        public void setEmptyView(Component component) {
            super.setEmptyView(component);
        }

        @Override
        public void refresh(boolean isAdd) {
            super.refresh(isAdd);
        }
    }
}
