package com.iwgang.familiarrecyclerviewdemo.slice;

import com.iwgang.familiarrecyclerview.FamiliarRecyclerView;
import com.iwgang.familiarrecyclerview.FamiliarWrapRecyclerViewAdapter;
import com.iwgang.familiarrecyclerviewdemo.ResourceTable;
import com.iwgang.familiarrecyclerviewdemo.util.ToastUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;

public class LinearLayoutAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private boolean                         isVertical = true;
    private FamiliarRecyclerView            mRecyclerView;
    private ArrayList<String>               mDatas;
    private FamiliarWrapRecyclerViewAdapter mFamiliarWrapRecyclerViewAdapter;
    private MyAdapter                       mAdapter;

    @Override
    protected void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_title_bar_bg).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }

        super.onStart(intent);
        if (intent != null) {
            isVertical = intent.getBooleanParam("flag", false);
        }
        if (isVertical) {
            super.setUIContent(ResourceTable.Layout_act_layout_linear_ver);
        } else {
            super.setUIContent(ResourceTable.Layout_act_layout_linear_hor);
        }
        initView();
    }

    private void initView() {
        // add data
        mDatas = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            mDatas.add("item:" + i);
        }
        Component recycleView = findComponentById(ResourceTable.Id_liner_mRecyclerView);
        if (recycleView instanceof FamiliarRecyclerView) {
            mRecyclerView = (FamiliarRecyclerView) recycleView;
        }
        mAdapter = new MyAdapter();
        mFamiliarWrapRecyclerViewAdapter = new FamiliarWrapRecyclerViewAdapter(mAdapter);

        // add headView
        addHeadView();

        // add footView
        addFootView();

        // add emptyView
        setEmptyView();

        mRecyclerView.setItemProvider(mFamiliarWrapRecyclerViewAdapter);

        setListener();
        // add item
        Component add = findComponentById(ResourceTable.Id_tv_add);
        if (add instanceof Text) {
            Text tvAdd = (Text) add;
            tvAdd.setClickedListener(this);
        }
        // delete item
        Component delete = findComponentById(ResourceTable.Id_tv_delete);
        if (delete instanceof Text) {
            Text tvDelete = (Text) delete;
            tvDelete.setClickedListener(this);
        }
    }

    private void setListener() {
        mFamiliarWrapRecyclerViewAdapter.setOnItemClickListener(
                (component, position) -> {
                    int adapterCount = mFamiliarWrapRecyclerViewAdapter.getCount();
                    if ((position != 0) && (position != (adapterCount - 1))) {
                        ToastUtil.makeText(getContext(), "onItemClick = " + (position - 1),
                                LayoutAlignment.BOTTOM);
                    }
                });

        mFamiliarWrapRecyclerViewAdapter.setOnItemLongClickListener(
                (component, position) -> {
                    int adapterCount = mFamiliarWrapRecyclerViewAdapter.getCount();
                    if ((position != 0) && (position != (adapterCount - 1))) {
                        ToastUtil.makeText(getContext(), "onItemLongClick = " + (position - 1),
                                LayoutAlignment.BOTTOM);
                    }
                });
    }

    private void setEmptyView() {
        LayoutScatter mLayoutScatter3 = LayoutScatter.getInstance(this);
        if (isVertical) {
            mFamiliarWrapRecyclerViewAdapter.setEmptyView(mLayoutScatter3
                    .parse(ResourceTable.Layout_empty_view, null, false));
        } else {
            mFamiliarWrapRecyclerViewAdapter.setEmptyView(mLayoutScatter3
                    .parse(ResourceTable.Layout_empty_view_hor, null, false));
        }
    }

    private void addFootView() {
        LayoutScatter mLayoutScatter2 = LayoutScatter.getInstance(this);
        if (isVertical) {
            mFamiliarWrapRecyclerViewAdapter.addFooterView(mLayoutScatter2
                    .parse(ResourceTable.Layout_view_footer_ver, null, false));
        } else {
            mFamiliarWrapRecyclerViewAdapter.addFooterView(mLayoutScatter2
                    .parse(ResourceTable.Layout_view_footer_hor, null, false));
        }
    }

    private void addHeadView() {
        LayoutScatter mLayoutScatter = LayoutScatter.getInstance(this);
        if (isVertical) {
            mFamiliarWrapRecyclerViewAdapter.addHeaderView(mLayoutScatter
                    .parse(ResourceTable.Layout_view_header_ver, null, false));
        } else {
            mFamiliarWrapRecyclerViewAdapter.addHeaderView(mLayoutScatter
                    .parse(ResourceTable.Layout_view_header_hor, null, false));
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_tv_add:
                if (mDatas != null) {
                    mDatas.add("new add item:" + mDatas.size());
                    mAdapter.notifyDataChanged();
                    mFamiliarWrapRecyclerViewAdapter.notifyDataChanged();
                }
                break;
            case ResourceTable.Id_tv_delete:
                if (mDatas.size() >= 1) {
                    mDatas.remove(mDatas.size() - 1);
                    mAdapter.notifyDataChanged();
                    mFamiliarWrapRecyclerViewAdapter.notifyDataChanged();
                }
                break;
        }
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackPressed() {
        terminateAbility();
        super.onBackPressed();
    }

    private class MyAdapter extends BaseItemProvider {
        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
            MyHolder holder = null;
            String data = mDatas.get(position);
            if ((component == null) || (component.getTag() == null)) {
                int layoutId = isVertical ? ResourceTable.Layout_item_view_linear_ver :
                        ResourceTable.Layout_item_view_linear_hor;
                component = LayoutScatter.getInstance(getContext())
                        .parse(layoutId, null, false);
                holder = new MyHolder(component);
                component.setTag(holder);
            } else {
                Object tag = component.getTag();
                if (tag instanceof MyHolder) {
                    holder = (MyHolder) tag;
                }
            }
            if (holder != null) {
                holder.mTvTxt.setText(data);
                if (position != 0) {
                    holder.mLine.setVisibility(Component.VISIBLE);
                } else {
                    holder.mLine.setVisibility(Component.HIDE);
                }
            }
            return component;
        }
    }

    private static class MyHolder {
        private         Text      mTvTxt;
        private final Component mLine;

        public MyHolder(Component component) {
            Component txt = component.findComponentById(ResourceTable.Id_tv_txt);
            if (txt instanceof Text) {
                mTvTxt = (Text) txt;
            }
            mLine = component.findComponentById(ResourceTable.Id_cp_line);
        }
    }

}
