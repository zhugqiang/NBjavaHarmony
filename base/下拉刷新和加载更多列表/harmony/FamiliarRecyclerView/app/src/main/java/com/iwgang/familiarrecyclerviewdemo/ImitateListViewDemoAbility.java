package com.iwgang.familiarrecyclerviewdemo;

import com.iwgang.familiarrecyclerviewdemo.slice.ImitateListViewDemoAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ImitateListViewDemoAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ImitateListViewDemoAbilitySlice.class.getName());
    }
}
