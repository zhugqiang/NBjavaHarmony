package com.iwgang.familiarrecyclerviewdemo;

import com.iwgang.familiarrecyclerviewdemo.slice.LinearLayoutAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class LinearLayoutAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(LinearLayoutAbilitySlice.class.getName());
    }
}
