package com.iwgang.familiarrecyclerviewdemo.slice;

import com.iwgang.familiarrecyclerviewdemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_title_bar_bg).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        Component llVer = findComponentById(ResourceTable.Id_linearLayout_ver);
        if (llVer instanceof Button) {
            Button linearLayoutVer = (Button) llVer;
            linearLayoutVer.setText(String.format("LinearLayout %n ListView (Vertical)"));
            linearLayoutVer.setClickedListener(this);
        }
        Component llHor = findComponentById(ResourceTable.Id_linearLayout_hor);
        if (llHor instanceof Button) {
            Button linearLayoutHor = (Button) llHor;
            linearLayoutHor.setText(String.format("LinearLayout %n ListView (Horizontal)"));
            linearLayoutHor.setClickedListener(this);
        }
        Component glVer = findComponentById(ResourceTable.Id_gridLayout_ver);
        if (glVer instanceof Button) {
            Button gridLayoutVer = (Button) glVer;
            gridLayoutVer.setText(String.format("GridLayout %n GridView (Vertical)"));
            gridLayoutVer.setClickedListener(this);
        }
        Component glHor = findComponentById(ResourceTable.Id_gridLayout_hor);
        if (glHor instanceof Button) {
            Button gridLayoutHor = (Button) glHor;
            gridLayoutHor.setText(String.format("GridLayout %n GridView (Horizontal)"));
            gridLayoutHor.setClickedListener(this);
        }
        Component imList = findComponentById(ResourceTable.Id_imitate_listView);
        if (imList instanceof Button) {
            Button imitateListView = (Button) imList;
            imitateListView.setClickedListener(this);
        }
        Component imGrid = findComponentById(ResourceTable.Id_imitate_gridView);
        if (imGrid instanceof Button) {
            Button imitateGridView = (Button) imGrid;
            imitateGridView.setClickedListener(this);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_linearLayout_ver:
                goToLinearVer();
                break;
            case ResourceTable.Id_linearLayout_hor:
                goToLinearHor();
                break;
            case ResourceTable.Id_gridLayout_ver:
                goToGridVer();
                break;
            case ResourceTable.Id_gridLayout_hor:
                goToGridHor();
                break;
            case ResourceTable.Id_imitate_listView:
                goToImitateList();
                break;
            case ResourceTable.Id_imitate_gridView:
                goToImitateGrid();
                break;
        }
    }

    private void goToImitateGrid() {
        Intent intent6 = new Intent();
        Operation operation6 = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.iwgang.familiarrecyclerviewdemo")
                .withAbilityName("com.iwgang.familiarrecyclerviewdemo.ImitateGridViewDemoAbility")
                .build();
        intent6.setOperation(operation6);
        startAbility(intent6);
    }

    private void goToImitateList() {
        Intent intent5 = new Intent();
        Operation operation5 = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.iwgang.familiarrecyclerviewdemo")
                .withAbilityName("com.iwgang.familiarrecyclerviewdemo.ImitateListViewDemoAbility")
                .build();
        intent5.setOperation(operation5);
        startAbility(intent5);
    }

    private void goToLinearVer() {
        Intent intent1 = new Intent();
        Operation operation1 = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.iwgang.familiarrecyclerviewdemo")
                .withAbilityName("com.iwgang.familiarrecyclerviewdemo.LinearLayoutAbility")
                .build();
        intent1.setParam("flag" , true);
        intent1.setOperation(operation1);
        startAbility(intent1);
    }

    private void goToLinearHor() {
        Intent intent2 = new Intent();
        Operation operation2 = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.iwgang.familiarrecyclerviewdemo")
                .withAbilityName("com.iwgang.familiarrecyclerviewdemo.LinearLayoutAbility")
                .build();
        intent2.setParam("flag" , false);
        intent2.setOperation(operation2);
        startAbility(intent2);
    }

    private void goToGridVer() {
        Intent intent3 = new Intent();
        Operation operation3 = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.iwgang.familiarrecyclerviewdemo")
                .withAbilityName("com.iwgang.familiarrecyclerviewdemo.GridLayoutAbility")
                .build();
        intent3.setParam("flag" , true);
        intent3.setOperation(operation3);
        startAbility(intent3);
    }

    private void goToGridHor() {
        Intent intent4 = new Intent();
        Operation operation4 = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.iwgang.familiarrecyclerviewdemo")
                .withAbilityName("com.iwgang.familiarrecyclerviewdemo.GridLayoutAbility")
                .build();
        intent4.setParam("flag" , false);
        intent4.setOperation(operation4);
        startAbility(intent4);
    }
}
