package com.iwgang.familiarrecyclerviewdemo;

import com.iwgang.familiarrecyclerviewdemo.slice.ImitateGridViewDemoAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ImitateGridViewDemoAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ImitateGridViewDemoAbilitySlice.class.getName());
    }
}
