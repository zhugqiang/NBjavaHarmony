package com.iwgang.familiarrecyclerviewdemo;

import com.iwgang.familiarrecyclerviewdemo.slice.GridLayoutAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class GridLayoutAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(GridLayoutAbilitySlice.class.getName());
    }
}
