/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.iwgang.familiarrecyclerviewdemo.util;

import com.iwgang.familiarrecyclerviewdemo.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * ToastUtil
 */
public class ToastUtil {
    /**
     * makeText
     *
     * @param context context
     * @param text text
     * @param layoutAlignment layoutAlignment
     */
    public static void makeText(Context context, String text, int layoutAlignment) {
        Component toastLayout = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_layout_toast,
                        null, false);
        new ToastDialog(context)
                .setComponent(toastLayout)
                .setText(text)
                .setCornerRadius(60)
                .setOffset(0, 100)
                .setAlignment(layoutAlignment)
                .show();
    }
}
