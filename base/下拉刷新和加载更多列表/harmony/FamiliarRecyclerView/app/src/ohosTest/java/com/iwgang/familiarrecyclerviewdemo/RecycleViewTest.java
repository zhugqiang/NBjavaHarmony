/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.iwgang.familiarrecyclerviewdemo;

import com.iwgang.familiarrecyclerview.ResourceTable;
import com.iwgang.familiarrecyclerviewdemo.util.DimenUtil;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * 单元测试
 */
public class RecycleViewTest {
    /**
     * 入口
     */
    @Test
    public void onStart() {
    }

    /**
     * 测试DimenUtil.getDimenValue()方法获取数据是否正确
     */
    @Test
    public void TestDimen() {
        Context appContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int dimenValue = DimenUtil.getDimenValue(appContext, 10);
        assertThat(dimenValue, is(30));
    }

    /**
     * 测试DimenUtil.getDimen()方法获取数据是否正确
     */
    @Test
    public void TestDimenRes() {
        Context appContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int dimen = DimenUtil.getDimen(appContext, ResourceTable.Integer_load_text_size);
        assertThat(dimen, is(48));
    }
}
