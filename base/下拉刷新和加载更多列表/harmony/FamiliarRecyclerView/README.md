# FamiliarRecyclerView

#### 项目介绍
- 项目名称：FamiliarRecyclerView
- 所属系列：openharmony的第三方组件适配移植
- 功能：ListContainer实现横竖列表,横竖网格视图以及下拉刷新和加载更多
- 项目移植状态：主要功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.3.5

#### 效果演示
![@iwgang](https://images.gitee.com/uploads/images/2021/0624/165615_e530e13b_8997284.gif "RecycleView1.gif")

![@iwgang](https://images.gitee.com/uploads/images/2021/0624/165630_9c13fa24_8997284.gif "RecycleView2.gif")

![@iwgang](https://images.gitee.com/uploads/images/2021/0624/165643_10f649c2_8997284.gif "RecycleView3.gif")

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:FamiliarRecyclerView:0.0.1-SNAPSHOT')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
```
// 添加/删除 头部View
mFamiliarWrapRecyclerViewAdapter.addHeaderView() 和 .removeHeaderView()

// 添加/删除 底部View
mFamiliarWrapRecyclerViewAdapter.addFooterView() 和 .removeFooterView()

// 设置数据空View
mFamiliarWrapRecyclerViewAdapter.setEmptyView()

// Item单击事件
mFamiliarWrapRecyclerViewAdapter.setOnItemClickListener(
                (component, position) -> {
                    return true;
                });

// Item长按事件
mFamiliarWrapRecyclerViewAdapter.setOnItemLongClickListener(
                (component, position) -> {
                    return true;
                });

// 上拉加载
mFamiliarRefreshRecyclerView.setOnLoadMoreListener(new FamiliarRefreshRecyclerView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                
            }
        });

// 下拉刷新
mFamiliarRefreshRecyclerView.setOnPullRefreshListener(new FamiliarRefreshRecyclerView.OnPullRefreshListener() {
            @Override
            public void onPullRefresh() {
                
            }
        });

// 设置加载更多的View
FamiliarRefreshRecyclerView.setLoadMoreView(...)

// 设置启动/停用下拉刷新
FamiliarRefreshRecyclerView.setLoadMoreEnabled(true / false);

// 设置启动/停用加载更多
FamiliarRefreshRecyclerView.setPullRefreshEnabled(true / false);

```


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 待实现

- 瀑布流布局

- 嵌套滑动效果

- 网格布局的点击事件

#### 版本迭代

- 0.0.1-SNAPSHOT


