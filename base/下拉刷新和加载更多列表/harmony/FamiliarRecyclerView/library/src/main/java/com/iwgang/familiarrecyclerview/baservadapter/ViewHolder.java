/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.iwgang.familiarrecyclerview.baservadapter;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.HashMap;

/**
 * ViewHolder
 */
public class ViewHolder {
    private final HashMap<Integer, Component> mViews;
    private final int mPosition;
    private final Component mConvertView;
    private final int mLayoutId;

    public ViewHolder(Context context, int layoutId, int position) {
        mLayoutId = layoutId;
        this.mPosition = position;
        this.mViews = new HashMap<>();
        mConvertView = LayoutScatter.getInstance(context)
                .parse(layoutId, null, false);
        mConvertView.setTag(this);
    }

    /**
     * getViewHolder
     *
     * @param context     context
     * @param convertView convertView
     * @param layoutId    layoutId
     * @param position    position
     * @return ViewHolder
     */
    public static ViewHolder get(
            Context context, Component convertView, int layoutId, int position) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder(context, layoutId, position);
        } else {
            Object tag = convertView.getTag();
            if (tag instanceof ViewHolder) {
                holder = (ViewHolder) tag;
            }
        }
        return holder;
    }

    /**
     * getPosition
     *
     * @return int
     */
    public int getPosition() {
        return mPosition;
    }

    /**
     * getLayoutId
     *
     * @return int
     */
    public int getLayoutId() {
        return mLayoutId;
    }

    /**
     * 通过viewId获取控件
     *
     * @param viewId viewId
     * @param <T>    Component T
     * @return T extends Component
     */
    public <T extends Component> T getView(int viewId) {
        Component view = mViews.get(viewId);
        if (view == null) {
            view = mConvertView.findComponentById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 获取转换视图.
     *
     * @return Component
     */
    public Component getConvertView() {
        return mConvertView;
    }

    /**
     * setVisible
     *
     * @param viewId  viewId
     * @param visible visible
     * @return ViewHolder
     */
    public ViewHolder setVisible(int viewId, boolean visible) {
        Component view = getView(viewId);
        view.setVisibility(visible ? Component.VISIBLE : Component.HIDE);
        return this;
    }

    /**
     * setInvisible
     *
     * @param viewId    viewId
     * @param invisible invisible
     * @return ViewHolder
     */
    public ViewHolder setInvisible(int viewId, boolean invisible) {
        Component view = getView(viewId);
        view.setVisibility(invisible ? Component.VISIBLE : Component.INVISIBLE);
        return this;
    }

    /**
     * 关于事件
     *
     * @param viewId   viewId
     * @param listener listener
     * @return ViewHolder
     */
    public ViewHolder setClickedListener(int viewId, Component.ClickedListener listener) {
        Component view = getView(viewId);
        view.setClickedListener(listener);
        return this;
    }

    /**
     * setTouchEventListener
     *
     * @param viewId   viewId
     * @param listener listener
     * @return ViewHolder
     */
    public ViewHolder setTouchEventListener(int viewId, Component.TouchEventListener listener) {
        Component view = getView(viewId);
        view.setTouchEventListener(listener);
        return this;
    }

    /**
     * setLongClickedListener
     *
     * @param viewId   viewId
     * @param listener listener
     * @return ViewHolder
     */
    public ViewHolder setLongClickedListener(int viewId, Component.LongClickedListener listener) {
        Component view = getView(viewId);
        view.setLongClickedListener(listener);
        return this;
    }
}
