package com.iwgang.familiarrecyclerview;

import ohos.agp.components.*;
import ohos.app.Context;

/**
 * FamiliarDefaultLoadMoreView
 * Created by iWgang on 17/1/2.
 * https://github.com/iwgang/FamiliarRecyclerView
 */
public class FamiliarDefaultLoadMoreView extends DirectionalLayout {
    private DependentLayout mContentView;
    private Component       mComponent;

    public FamiliarDefaultLoadMoreView(Context context) {
        this(context , null);
    }

    public FamiliarDefaultLoadMoreView(Context context, AttrSet attrSet) {
        this(context , attrSet , "");
    }

    public FamiliarDefaultLoadMoreView(Context context, AttrSet attrSet, String styleName) {
        super(context , attrSet , styleName);
        setContent(context);
    }

    private void setContent(Context context) {
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_recycler_layout, this, true);
        Component list = findComponentById(ResourceTable.Id_list_layout);
        if (list instanceof DependentLayout) {
            mContentView = (DependentLayout) list;
        }
    }

    public void addContentView(Component contentView) {
        if (contentView != null) {
            mComponent = contentView;
            mContentView.addComponent(mComponent);
        }
    }

    public void removeContentView(Component contentView) {
        if (contentView != null) {
            mContentView.removeComponent(contentView);
        }
    }

    public Component getContentView() {
        return mComponent;
    }
}
