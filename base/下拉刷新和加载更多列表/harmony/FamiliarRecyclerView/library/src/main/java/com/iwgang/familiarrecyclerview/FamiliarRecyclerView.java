package com.iwgang.familiarrecyclerview;

import ohos.agp.components.*;
import ohos.app.Context;

/**
 * FamiliarRecyclerView
 * Created by iWgang on 15/10/31.
 * https://github.com/iwgang/FamiliarRecyclerView
 */
public class FamiliarRecyclerView extends ListContainer {
    public FamiliarRecyclerView(Context context) {
        this(context, null);
    }

    public FamiliarRecyclerView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public FamiliarRecyclerView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void setItemProvider(BaseItemProvider itemProvider) {
        super.setItemProvider(itemProvider);
    }

    @Override
    protected boolean onItemLongClicked(ListContainer parent, Component component, int position, long id) {
        return false;
    }

    @Override
    public void setDraggedListener(int dragMode, DraggedListener listener) {
        super.setDraggedListener(DragEvent.DRAG_BEGIN, new DraggedListener() {
            @Override
            public void onDragDown(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragStart(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragUpdate(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragEnd(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragCancel(Component component, DragInfo dragInfo) {
            }

            @Override
            public boolean onDragPreAccept(Component component, int dragDirection) {
                return false;
            }
        });
    }
}
