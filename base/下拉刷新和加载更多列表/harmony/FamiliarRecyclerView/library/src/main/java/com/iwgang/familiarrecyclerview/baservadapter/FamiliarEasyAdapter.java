package com.iwgang.familiarrecyclerview.baservadapter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import ohos.utils.PlainArray;

import java.util.List;

/**
 * FamiliarEasyAdapter
 */
public class FamiliarEasyAdapter<T> extends FamiliarBaseAdapter<T> {
    private AbilitySlice mSlice;
    private int mItemLayoutId;

    public FamiliarEasyAdapter(AbilitySlice slice, int itemLayoutId) {
        this(slice, null , itemLayoutId);
    }

    public FamiliarEasyAdapter(AbilitySlice slice, List<T> data , int itemLayoutId) {
        super(slice.getContext() , data , itemLayoutId);
        mSlice = slice;
        mItemLayoutId = itemLayoutId;
    }

    @Override
    public void convert(com.iwgang.familiarrecyclerview.baservadapter.ViewHolder holder, T t) {
    }

    public static class ViewHolder extends com.iwgang.familiarrecyclerview.baservadapter.ViewHolder {
        private PlainArray<Component> mViews;
        private Component mItemView;

        public ViewHolder(Context context, ComponentContainer parent, int layoutId, int position) {
            super(context, layoutId, position);
            mViews = new PlainArray<>();
            mItemView = parent.findComponentById(layoutId);
        }

        public <T extends Component> T findView(int viewId) {
            Component view = mViews.get(viewId).get();
            if (view == null) {
                view = mItemView.findComponentById(viewId);
                mViews.put(viewId, view);
            }
            return (T) view;
        }
    }
}
