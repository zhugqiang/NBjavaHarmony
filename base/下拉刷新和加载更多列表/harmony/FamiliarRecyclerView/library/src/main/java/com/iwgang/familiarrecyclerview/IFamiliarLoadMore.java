package com.iwgang.familiarrecyclerview;

import ohos.agp.components.Component;

/**
 * LoadMoreView interface
 * Created by iWgang on 16/04/13.
 * https://github.com/iwgang/FamiliarRecyclerView
 */
public interface IFamiliarLoadMore {
    void showLoading();

    void showNormal();

    boolean isLoading();

    Component getView();
}
