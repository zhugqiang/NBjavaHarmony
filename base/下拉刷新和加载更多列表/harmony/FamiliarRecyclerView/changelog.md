## 0.0.1-SNAPSHOT
FamiliarRecyclerView组件openharmony第一个beta版本

#### 已实现功能

* ListView / GridView布局
* 上拉加载
* 下拉刷新

#### api差异

* 原库的方法都是通过RecyclerView调用,openharmony的方法是通过adapter调用

#### 未实现功能

* 瀑布流布局
* 嵌套滑动效果
* 网格布局的点击事件
