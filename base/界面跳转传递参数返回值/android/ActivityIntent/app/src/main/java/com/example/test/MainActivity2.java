package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    private Button btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        initView();
        initEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent=new Intent();
        intent.putExtra("rs","我来自页面一");
        setResult(100,intent);
    }

    private void initView() {
        btnBack=(Button)findViewById(R.id.btnBack);
    }
    private void initEvent() {

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

}