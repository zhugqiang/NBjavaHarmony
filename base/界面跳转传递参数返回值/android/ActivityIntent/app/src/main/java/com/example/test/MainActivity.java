package com.example.test;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btnGotoPage1;
    private Button btnGotoPage2;
    private TextView txtParam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initEvent();
    }

    private void initEvent() {
        btnGotoPage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent();
                intent.setClass(MainActivity.this,MainActivity2.class);
                startActivityForResult(intent,100);

            }
        });
        btnGotoPage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent();
                intent.setClass(MainActivity.this,MainActivity3.class);
                startActivityForResult(intent,100);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100){

            String param=data.getStringExtra("rs");
            txtParam.setText(param);
        }
    }

    private void initView() {
        btnGotoPage1=(Button)findViewById(R.id.btnGotoPage1);
        btnGotoPage2=(Button)findViewById(R.id.btnGotoPage2);
        txtParam=(TextView) findViewById(R.id.txtParam);
    }
}